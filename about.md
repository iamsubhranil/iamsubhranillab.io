---
layout: page
title: About
permalink: /about/
---

`v0.0`

I'm Subhranil Mukherjee, a computer enthusiast by birth and programmer by passion.
I'm currently completing my major from the University of Calcutta on, you guessed it, computer science.
Most of the time I'm fiddling with my laptop, be it learning a new language, or writing a new one.
You can find me on handle `iamsubhranil` at most places where I am.
I like learning about everything science, especially about astrophysics : outer space, universe and stuff.
So hook me up on any cool stuff you find interesting on `/u/iamsubhranil`.

Also, for me it's `vim`. It's always been, and will be for the forseeable future. ;)

P. S. : This might not be the most interesting about you can find on the internet, but I'm hoping it will improve on the next version. :)
