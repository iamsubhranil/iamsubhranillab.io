---
layout: post
title: Initial Commit
---


I've always wanted a place where I can record my learnings in a human readable version, be it for my future self, or for any one of you.
I've fiddled with wordpress and friends before, but they never attracted me that much. I felt like I have less control over stuff. That was
a while ago.
I tried posting a bit of content to some popular websites out there, but all of them got rejected for not suiting their genre.
I also had a GitHub page intially, but left it for not having enough time at that time.

Recently there is a huge trend of choosing GitLab over GitHub between people, and that has its fair share of reasons.
I don't want to repeat them, but recently I started transitioning my projects to GitLab.
The only thing that it lacks is a good Android client IMO. The rest of the interface is pretty solid and modern, and
might be a bit more confusing to the newbies than GitHub.
Although I might have preferred a way to change the whole website interface to a darker theme, because its background is more
bright than GitHub, but I think it's just me (is it really though?).

Last night, after an hour or two, I finally managed to create a `GitLab page`, and today this is the first post on it.
I have final exams tomorrow, and I can't really think of a better way to rather spend my time on.

So, that's a lot of information. I will be posting anything new I learn in here from now on, and some general posts
to lessen the fear of programming among general people, which I have been planning to do for some time. Also, some random
posts will also make their way here, as you will expect from a random guy at the internet, and now that I think about it,
trying to categorize my future posts was really a bad idea to begin with.

'Till next time. :)

---
