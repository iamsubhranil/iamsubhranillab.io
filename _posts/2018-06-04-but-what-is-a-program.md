---
layout: post
title: But what *is* a program?
---

Look around you. What do you see? Well, people mostly. But also, an exponentially increasing lot of computers.
They're everywhere. Almost in everything that runs on electricity. That's good, and a bit frightening, but hey,
they help you a awful lot.

But they're dumb. Period. They can't do one thing unless it is explicitly told to them.

> Although, that thing is slowly changing now, but that is a whole other issue. 

So, what's their secret sauce? What makes them so *helpful*? And hey, why do we *need* a computer
at all?

Let's start from the beginning.

## The Premise

We are already good at calculations. We don't need external help of any kind to calculate <code>2 + 5</code>. 

Some may say that was way too easy. Fair enough. How about <code>11 x 13</code>? That was a bit harder than the previous. 
But that was easy too. Let's up the game.

What is <code>132 x 462</code>? This might take a minute to do by hand. But it is still doable. 

Alright then, what is <code>388126 x 481924</code>? Things are getting tougher now, isn't it? 
We can still do it, but imagine tens of them coming towards you at one second. You have to either be specially trained, or be a prodigy.

## The Problem

So, we can do mathematical calculations, we can even do some tough ones, but the problem is the majority of 
us will hit a roadblock if things start to get a bit large and speedy. 
I mean we could still do that, but by the time we'll be finished with it, we'll probably lose the value of 
the reason for which we started the calculation at first. 
So, we need a helping hand. 
We need some *thing* that'll do the calculations for us, and hopefully will be speedier than us too.

> The reason we're bothered about doing fast, efficient mathematical calculation is that everything can be 
> expressed in mathematical terms, from having a [perfect cheese on toast](https://www.independent.co.uk/news/science/perfect-cheese-on-toast-if-bbread-ccheese-and-ttime-what-is-the-point-of-all-these-formulae-8773470.html), to travelling to outer space.

## The (Initial) Solution

We are pretty good at making machines, and surely enough, we can jolt some bolts and wires to make some 
machines that'll perform all the additions subtractions and multiplications for us. Consider 
we've done that, and we've built 4 machines :

1. **MA** : The machine which carries instructions on how to perform addition of two numbers. 
The machine takes two integers, processes them, i.e. performs decimal addition, and gives back another integer, i.e. the sum, as result.
2. **MS** : The machine that knows how to subtract two integers. 
Like the addition machine, it takes in two integers, and gives back the difference as result.
3. **MM** : The machine that is soldered to multiply two integers. 
It takes input as previous, multiplies them, and returns back the result.
4. **MD** : The divider machine. Likewise, takes two integers, divides the first by the second, and returns back the result.

Our plan is ingenious, in so that it removes the hard task, the calculations, from us, and works like some 
magic wand which just joins two integers in whatever way it was made for to. 
They can, or particularly <b>MM</b> can, breeze through the task of <code>388126 x 481924</code> which previously had us sweats in palms. 

But hold on. How about mixed operations? <code>(4723381 + 9323940) x 882123</code>? 
That might seem a bit odd, but all we have to do is get the result of <code>4723381 + 9323940</code> 
from <b>MA</b>, the addition machine, and feed it to the <b>MM</b>, multiplication machine. 

> And that's enough math for one day.

When we do the calculation by hand, we still have to perform the exact three steps. 
But now we're performing one. We have lessen the work by 66%, by we still have to intervene. We're still *bottlenecking*.

> Bottleneck
>
> *noun*
>
>    a narrow section of road or a junction that impedes traffic flow.

Consider another scenario. We have to find the sine of a given angle. Our four machines do not do the job. 
So we invent a new one. And gradually we invent a hundred other for hundred different operations, 
and switch through them as we're performing calculations. By doing so, we're diminishing the performance 
gain that we were getting for simple calculations at first when the first four machines were introduced. 

Previously, we were lacking at processing the data. Now, the machines will sit idle most of the time, 
hence the usage of the total processing power at hand will be minimum, and most of the time we'll just 
nervously switch from one machine to another.

## The Solution (2.0)

If you look closely, you'll notice that the function that a machine performs, is bound by the set of instructions 
that were given to it *at the time of production*. 
The instructions are the things that label a machine as *The addition machine*, or *The multiplication machine*. 

So how about building such a machine, which also takes the instructions as the input? 

Consider a generic machine, which has some fundamental operations built-in, but it does nothing by itself unless specified. 
When we want the machine to perform something on two integers, like previously, we give it two integers. 
But this time, we give it an *additional* input, **a sequential set of instructions** that tells it what 
exactly should it do with the given integers. 

> Although, to be honest, addition, subtraction, multiplication and
> division are also some of the fundamental operations of a modern
> computer. But you get the idea.

We give it the data, and we give it the instructions on how to process the data alongside. 
We then build a general purpose computational device, or simply, a *computer*.

> Computer
> 
> *noun*
>
> an electronic device which is capable of receiving information (data) in a 
> particular form and of performing a sequence of operations in accordance with 
> a predetermined but variable set of procedural instructions (program) to produce a result in the form of information or signals.
>
> (Phew. That was a bad idea.)

Now let the idea sink in for a moment. We have a machine which takes the data as input, *as well as* the 
instructions on how to process them. 
That sequential set of instructions are generally referred to as a *program*, because it programs the device, i.e. controls its behaviour. 

> Program
>
> *noun* 
>
>    a planned series of future events or performances.

That solves our basic problem of having to maintain a larger array of specialized devices. 
When we want to perform a long operation using our new machine, at each step, we feed it the usual input, along with 
the instructions on processing them. And when they're done, we collect the output from them.

Still, we have to do one step at a time, since till now, we are the ones who are feeding the machine the inputs. 
We still will be limited by our capacity of feeding the machine, we still will bottleneck.

## The (Semifinal) Solution

We are inching towards our goal of performing calculations as fast as possible, but we still can't remove *us* from 
the equation. Our new generic machine is now mostly perfect. It just requires some final touches. 

How about, we store all the steps that are need to be done along with the numbers on which the steps are to be performed 
*somewhere apriori to the calculation*, and tell the <b>Processor</b> (well of course, it processes everything) to fetch 
and process them *from there* in its own time?

> Processor
>
> *noun*
>
>    a machine that processes something.
>
> (Mine was original, I swear.)

We just need to make the processor understand how to fetch them, which is not a problem, since we can tell it now *how to* process 
a thing along with *what*. 

That storage where the steps and inputs are apriori loaded, is known as the <b>Memory</b>, since it helps the processor remember what to do next. 

> As you will see, the term *memory* for computers is quite analogous to the
> way we think about *memory of a human being*. They only difference being
> we know everything about the memory of a computer, i.e. how it can be stored
> and retrieved, and almost nothing about the memory of a human being.
> Quite ironical, isn't it?

We will now load the numbers and steps that are to be performed on them in memory, turn the processor on, and collect the results from the 
memory when the processor is finished.

The memory will be the bridge between us and the processor. 
It will be much faster than us, and we will still rip all the benefits that we are getting from a generic processor. 
If we want to add something, we will write the steps for addition, along with the numbers to add, load them into 
memory, and turn the processor on.

And bam! We will get the result instantly (or however fast the first generational CPUs were).

> There are a few different kind of memories that a computer can have,
> ordered by the speed by which they can be accessed and the amount of
> data they can store. Memory is costly, and hence the memory with
> highest access speed usually has the least storage space.
>
> Think of it in a humanly manner. The memories that we access frequently,
> we don't have to put a lot of pressure to bring them on. They just kind
> of come out of us. But there are much less things that you need to
> access from your memory in regular basis and in an intricate manner,
> like the password of your bank account. They come in short bursts.
>
> That awesome party that you went last time, you remember about it
> for a longer duration, but you don't remember all the intricate details
> about it at all.
>
> It's important to priotize what to remember, and also what to
> forget. Your brain does this for you when you sleep, a bit of
> housekeeping, to clean out the junk of the day and keep the
> most important and relevant bits of it stored in safe places.
>
> A computer cannot just clean out your data. It's *your* data after
> all. It has to decide what's more important, what it has to 
> recall very frequently, and move it to a faster storage so that
> the recall operation itself takes less time, and it can concentrate
> on actually processing the data. The data that was in the faster 
> storage earlier would then get moved to the slower storage. 
> A computer does this all the time, as *what is important* changes 
> quite frequently according to a particular context.
>
> We'll talk about it more, but for now, let's come back to
> the problem at hand - efficiently solving calculations as
> fast as possible.

The machine we designed is still *not perfect* though. 
In one sense that we have to specify *all* the steps for addition *each time* we want to add something, among others. 
That'll increase both our efforts and the processors.

So, what should be the remedy then?

'Till next time.

---
